import React, { ReactElement } from 'react'
import Box from '@mui/material/Box'
import Header from './components/Header'
import Footer from './components/Footer'

interface Props {
  children?: React.Component | ReactElement
}

const Index: React.FC<Props> = ({ children }) => {

  return (
    <>
      <Box padding={0} sx={{ flexGrow: 1,display: 'flex', flexDirection: 'column', minHeight: '100vh' }}>
        <Header />
        <Box padding={0} style={{flex: 1}}>
          {children}
        </Box>
        <Footer/>
      </Box>
    </>
  )
}

export default Index