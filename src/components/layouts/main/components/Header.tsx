import React, { useEffect, useMemo, useState } from 'react'
import { useRouter } from 'next/router'
import { Container, Grid, Box, Menu, MenuItem, IconButton } from '@mui/material/'
import { Logo, Hamburger, IconChallenge, IconEdit, IconInfo } from '~/components/elements/icons'
import styles from "./styles"

export const MAX_WIDTH_MOBILE = 880

const Header: React.FC = () => {
  const router = useRouter()
  const [isMobile, setIsMobile] = useState<boolean>(false)

  const listMenuItem = useMemo(() => {
    return [
      {
        name: '自分の日記',
        onClick: () => { router.push('/my-record') }
      },
      {
        name: '体調グラフ',
        onClick: () => { }
      },
      {
        name: '目標',
        onClick: () => { }
      },
      {
        name: '選択中コース',
        onClick: () => { }
      },
      {
        name: 'コラム一覧',
        onClick: () => { }
      },
      {
        name: '設定',
        onClick: () => { }
      },
    ]
  }, [])

  useEffect(() => {
    onChangeWidth()
    window.addEventListener('resize', onChangeWidth)
    return () => {
      window.removeEventListener('resize', onChangeWidth)
    }
  }, [])

  const onChangeWidth = () => {
    setIsMobile(window.innerWidth <= MAX_WIDTH_MOBILE)
  }

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Container disableGutters={true} maxWidth={false}>
      <Grid style={styles.menu} container spacing={0}>
        <Grid style={{ display: 'flex' }} alignItems="center" item xs={3}>
          <Box onClick={() => { router.push('/') }} style={styles.logoContainer}>
            <Logo />
          </Box>
        </Grid>
        <Grid style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'center' }} alignItems="center" alignContent="flex-end" item xs={9}>
          {!isMobile && (
            <>
              <Box onClick={() => { router.push('/my-record') }} style={styles.menuItem}>
                <IconEdit />
                <span style={styles.menuText}>自分の日記</span>
              </Box>
              <Box style={styles.menuItem}>
                <IconChallenge />
                <span style={styles.menuText}>チャレンジ</span>
              </Box>

              <Box style={styles.menuItem}>
                <IconInfo />
                <span style={styles.menuText}>お知らせ</span>
              </Box>
            </>
          )}
          <Box>
            <IconButton
              onClick={handleClick}
              size="small"
              sx={{ ml: 2 }}
              aria-controls={open ? 'account-menu' : undefined}
              aria-haspopup="true"
              aria-expanded={open ? 'true' : undefined}
            >
              <Hamburger />
            </IconButton>
            <Menu
              anchorEl={anchorEl}
              id="account-menu"
              open={open}
              onClose={handleClose}
              onClick={handleClose}
              PaperProps={{
                elevation: 0,
                sx: styles.hamburger,
              }}
              style={styles.subMenu}
              MenuListProps={{ disablePadding: true }}
              transformOrigin={{ horizontal: 'right', vertical: 'top' }}
              anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
            >
              {listMenuItem.map((menuItem, index) => {
                return (
                  <MenuItem key={index} style={styles.subMenuItem} onClick={menuItem.onClick}>{menuItem.name}</MenuItem>
                )
              })}
            </Menu>
          </Box>
        </Grid>
      </Grid>
    </Container>
  )
}

export default Header
