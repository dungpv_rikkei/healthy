import { colors } from "~/theme/variables/platform"

const styles = {
  menu: {
    height: 65,
    backgroundColor: colors.dark500,
    width: '100%',
    paddingLeft: '12.5%',
    paddingRight: '12.5%',
  },
  logoContainer: {
    height: 40,
    marginLeft: 15,
    cursor: 'pointer',
  },
  hamburger: {
    overflow: 'visible',
    filter: `drop-shadow(0px 2px 8px ${colors.gray400})`,
    mt: 1.5,
    '& .MuiAvatar-root': {
      width: 32,
      height: 32,
      ml: -0.5,
      mr: 1,
    },
    '&:before': {
    },
  },
  menuRightContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  menuText: {
    color: colors.light,
    marginLeft: 10,
    fontSize: 16,
  },
  menuItem: {
    marginRight: 25,
    display: 'flex',
    height: 26,
    cursor: 'pointer',
  },
  subMenuItem: {
    borderBottom: 1,
    borderColor: colors.light,
    backgroundColor: colors.gray400,
    color: colors.light,
    fontSize: 18,
    width: 280,
    paddingTop: 25,
    paddingBottom: 25,
  },
  subMenu: {
    paddingBottom: 0,
  },
  footerContainer: {
    height: 128,
    backgroundColor: colors.dark500,
    color: colors.light,   
    paddingLeft: '12.5%',
    paddingRight: '12.5%',
    display: 'flex',
    alignItems: 'center',
  },
  footerMenu: {
    display: 'flex', 
    flexDirection: 'row',
    justifyContent: 'center'
  },
  footerMenuItem: {
    fontSize: 11,
    marginRight: 40,
  },
}


export default styles