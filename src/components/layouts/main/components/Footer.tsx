import React from 'react'
import { Box, Container, Grid } from '@mui/material'
import styles from "./styles"

const Footer: React.FC = () => {

  const menuItems = [
    {
      name: '会員登録',
    },
    {
      name: '運営会社',
    },
    {
      name: '利用規約',
    },
    {
      name: '個人情報の取扱について',
    },
    {
      name: '特定商取引法に基づく表記',
    },
    {
      name: 'お問い合わせ',
    },
  ]

  return (
    <Container disableGutters={true} maxWidth={false} style={styles.footerContainer}>
      <Grid container style={styles.footerMenu as any} spacing={0}>
          {menuItems.map((menuItem, index) => {
            return <Box key={index} style={styles.footerMenuItem}>{menuItem.name}</Box>
          })}
      </Grid>
    </Container>
  )
}

export default React.memo(Footer)