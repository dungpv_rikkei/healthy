import React, { useState } from 'react'
import { Container,  Grid, CircularProgress, Box } from '@mui/material'
import { TYPE_FOOD, getFoodTypeLabel } from '~/models/food'
import { createTheme, ThemeProvider } from '@mui/material/styles'
import { DATA_TEMP_GRAPH } from '~/models/bodyRecord'
import originFoods from '../../../data/food.json'
import MenuFoodItem from '~/components/elements/MenuFoodItem'
import FoodItem from '~/components/elements/FoodItem'
import BodyRecordGraph from '~/components/widgets/BodyRecordGraph'
import Button from '~/components/elements/Button'
import styles from './styles'
import { colors } from '~/theme/variables/platform'

const typeFoods = [
  TYPE_FOOD.MORNING,
  TYPE_FOOD.LUNCH,
  TYPE_FOOD.DINNER,
  TYPE_FOOD.SNACK,
]

const theme = createTheme({
  palette: {
    secondary: {
      main: colors.light,
    },
  },
});

const Index: React.FC = () => {

  const [foods, setFoods] = useState(originFoods || [])

  const loadMoreFood = () => {
    setFoods(foods => [...foods, ...originFoods])
  }
  
  return (
    <ThemeProvider theme={theme}>
      <Container disableGutters={true} maxWidth={false}>
        <Grid container>
          <Grid style={{ display: 'flex', position: 'relative' }} alignItems="center" item xs={12} md={4}>
              <img style={styles.mainImage as any} src="/images/main_food.png" />
              <CircularProgress thickness={0.8} style={styles.centerElement} color="secondary" size={200} variant="determinate" value={75} />
              <Box style={{...styles.centerElement, display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
                <Box style={styles.date}>05/21</Box>
                <Box style={styles.percent}>75%</Box>
              </Box>
          </Grid>
          <Grid style={{ display: 'flex' }} alignItems="center" item xs={12} md={8}>
            <BodyRecordGraph data={DATA_TEMP_GRAPH}/>
          </Grid>
        </Grid>
        <Grid style={styles.contentPadding}>
          <Grid container style={styles.menuFoodContainer as any}>
            <Grid item xs={12} md={12} xl={9} style={{display: 'flex', flexWrap: 'wrap'}}>
            {typeFoods.map((typeFood, index) => {
              return (
                <Grid key={index} item xs={6} sm={3} md={3}>
                  <MenuFoodItem title={getFoodTypeLabel(typeFood)}  customStyle={styles.menuFoodItem} typeFood={typeFood} />
                </Grid>
              )})
            }
            </Grid>
          </Grid>
          <Grid container style={styles.foodContainer as any} >
            {foods.map((food, index) => {
              return (
                <Grid key={index} item style={styles.foodItem} lg={2} sm={3} xs={6} md={3} >
                  <FoodItem food={food} />
                </Grid>
              )
            })}
          </Grid>
          <Grid style={styles.loadMoreContainer as any}>
            <Button title="記録をもっと見る" onClick={loadMoreFood} />
          </Grid>

        </Grid>
      </Container>
    </ThemeProvider>
  )
}

export default Index