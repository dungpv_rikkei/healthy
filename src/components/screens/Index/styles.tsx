import { colors } from "~/theme/variables/platform"

const styles = {
  menuFoodItem: {
    padding: 7,
  },
  menuFoodContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 25,
    marginBottom: 25,
  },
  foodContainer: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  foodItem: {
    paddingBottom: 3,
    paddingRight: 7,
  },
  loadMoreContainer: {
    marginTop: 30,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 60,
  },
  contentPadding: {
    paddingLeft: '12.5%', 
    paddingRight: '12.5%'
  },
  mainImage: {
    width: '100%',
    height: 330,
    objectFit: 'cover'
  },
  centerElement: {
    position: 'absolute' as any,
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)',
  },
  date: {
    color:  colors.light,
    fontSize: 18,
  },
  percent: {
    color:  colors.light,
    fontSize: 22,
    marginLeft: 7,
  },
}

export default styles