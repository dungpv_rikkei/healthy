import React, { useMemo, useState } from 'react'
import { Box, Container, Grid } from '@mui/material'
import { DATA_TEMP_GRAPH } from '~/models/bodyRecord'
import exercises from '../../../data/exercise.json'
import originDiaries from '../../../data/diary.json'
import BodyRecordGraph from '~/components/widgets/BodyRecordGraph'
import TableExercise from '~/components/widgets/TableExercise'
import CategoryItem, { ICategoryItem } from '~/components/elements/CategoryItem'
import Button from '~/components/elements/Button'
import styles from './styles'
import { IDiary } from '~/models/Diary'

const Index: React.FC = () => {

  const [diaries, setDiaries] = useState(originDiaries)

  const categories: ICategoryItem[] = useMemo(() => {
    return [
      {
        image: '/images/category/body_record.png',
        title: 'BODY RECORD',
        subTitle: '自分のカラダの記録',
        onClick: () => { alert('自分のカラダの記録') }
      },
      {
        image: '/images/category/my_exercise.png',
        title: 'MY EXERCISE',
        subTitle: '自分の運動の記録',
        onClick: () => { alert('自分の運動の記録') },
      },
      {
        image: '/images/category/body_diary.png',
        title: 'MY DIARY',
        subTitle: '自分の日記',
        onClick: () => { alert('自分の日記') }
      }
    ]
  }, [])

  const loadMoreDiaries = () => {
    // set default 5 rows
    setDiaries((diaries) => [...diaries, ...originDiaries.slice(0, 6)])
  }

  return (
    <Container style={styles.contentPadding} disableGutters={true} maxWidth={false}>
      <Grid container style={styles.categoryItemContainer as any}>
        {categories.map((category, index) => {
          return (
            <Grid key={index}>
              <CategoryItem category={category} />
            </Grid>
          )
        })}
      </Grid>

      <Grid container style={styles.gridContainer}>
        <Grid style={styles.containerGraphHeader} item xs={12}>
          <Grid item style={styles.headerGraphLeft}>
              BODY<br />
              RECORD
          </Grid>
          <Grid item style={styles.headerGraphRight}>
            2021.05.21
          </Grid>
        </Grid>
        <BodyRecordGraph
          data={DATA_TEMP_GRAPH}
        />
      </Grid>

      <Grid container style={styles.gridContainer}>
        <TableExercise headerTime={'2021.05.21'} exercises={exercises} />
      </Grid>

      <Grid container style={styles.gridContainer}>
        <Grid item xs={12} style={{fontSize: 22, textAlign: 'left', marginBottom: 5,}}>MY DIARY</Grid>
        <Grid container>
          {diaries.map((diary: IDiary, index) => {
            const date = new Date(diary.created_at)
            let month: string | number = date.getUTCMonth() + 1
            if (month < 10) {
              month = '0' + month
            }
            let day: string | number = date.getUTCDate()
            if (day < 10) {
              day = '0' + day
            }
            const year = date.getUTCFullYear()
            const dateString = `${year}.${month}.${day}`
            const hour = date.getHours()
            const min = date.getMinutes()
            return (
              <Grid item key={index} xs={6} sm={4} md={3} xl={2}>
                <Box style={styles.diaryContent}>
                  <Grid style={styles.diaryDate}>{dateString}</Grid>
                  <Grid style={styles.diaryMin}>{hour}:{min}</Grid>
                  <Grid style={styles.diaryNote}>{diary.note}</Grid>
                </Box>
              </Grid>
            )
          })}
        </Grid>
      </Grid>
      <Grid container style={{...styles.gridContainer, marginBottom: 65}}>
          <Button title="自分の日記をもっと見る" onClick={loadMoreDiaries} />
      </Grid>
    </Container>
  )
}

export default Index