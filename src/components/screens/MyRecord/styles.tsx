import { colors } from "~/theme/variables/platform"

const styles = {
  contentPadding: {
    paddingLeft: '12.5%', 
    paddingRight: '12.5%'
  },
  categoryItemContainer: {
    display: 'flex', 
    marginTop: 50,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  gridContainer: {
    display: 'flex', 
    justifyContent: 'center', 
    marginTop: 50,
    marginBottom: 50,
  },
  diaryContent: {
    border: `1px solid ${colors.dark600}`,
    marginRight: 10,
    marginBottom: 10,
    padding: 15
  },
  diaryDate: {
    fontSize: 16,
  },
  diaryMin: {
    fontSize: 16,
    marginBottom: 15,
  },
  diaryNote: {
    marginBottom: 15,
  },
  containerGraphHeader: {
    display: 'flex',
    alignItems: 'center',
    backgroundColor: colors.dark600,
    paddingLeft: 15,
    paddingTop: 15,
  },
  headerGraphLeft: {
    color: colors.light,
    fontSize: 15,
    marginLeft: 15,
  },
  headerGraphRight: {
    color: colors.light,
    marginLeft: 30,
    fontSize: 22,
  },
}

export default styles