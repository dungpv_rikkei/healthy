import React, { ReactNode, useState } from 'react'
import { Box, Grid } from '@mui/material'
import { Line } from 'react-chartjs-2'
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js'
import { colors } from '~/theme/variables/platform'

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

interface Props {
  data: any
}

const options = {
  responsive: true,
  maintainAspectRatio: false,
  plugins: {
    legend: {
      position: 'top',
      display: false,
    },
  },
  scales: {
    y: {
      grid: {
        display: false,
        color: colors.gray400,
      },
      ticks: {
        display: false,
      },
    },
    x: {
      ticks: {
        color: colors.gray400,
      },
      grid: {
        color: colors.gray400,
      },
    }
  }
}

const DEFAULT_HEIGHT = 300
const DEFAULT_WIDTH = 300
const DEFAULT_PADDING_WIDTH = 60

const Index: React.FC<Props> = ({
  data,
}) => {

  const [width, setWidth] = useState(DEFAULT_WIDTH)  
  const onchangeBox = (node) => {
    if (node) {
      setWidth(node.getBoundingClientRect().width - DEFAULT_PADDING_WIDTH)
    }
  }

  return (
    <Grid item ref={onchangeBox} style={styles.graphContainer}>
      <Line
        data={data}
        width={width}
        height={DEFAULT_HEIGHT}
        style={{
          backgroundColor: colors.dark600,
        }}
        options={options as any}
      />
    </Grid>
  )
}
export default Index

const styles = {
  graphContainer: {
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: colors.dark600,
    width: '100%',
  }
}