import { Box, Grid } from '@mui/material'
import Head from 'next/head'
import React from 'react'
import { IExerciseAttributes, getExerciseTypeLabel } from '~/models/exercise'
import { colors } from '~/theme/variables/platform'

interface Props {
  exercises: IExerciseAttributes[]
  headerTime: string,
}

export default function ({ headerTime, exercises }: Props) {

  return (
    <>
    <Head>
      <link rel="stylesheet" href="/css/exercise.css"/>
    </Head>
    <Box padding={0} style={{flex: 1}}>
      <Grid container style={styles.boxContainer}>
          <Grid style={styles.containerHeader} item xs={12}>
            <Grid item style={styles.headerLeft}>
                MY<br />
                EXERCISE
            </Grid>
            <Grid item style={styles.headerRight}>
              {headerTime}
            </Grid>
          </Grid>
        <Grid container style={styles.tableContainer as any}>
          {exercises.map((exercise, index) => {
            return (
              <Grid key={index} xs={6} item style={styles.tableItem}>
                <Box style={styles.tableDetailItem as any}>
                  <Grid style={styles.mainInfo}>
                    <Box style={styles.typeExercise}>{getExerciseTypeLabel(exercise.type)}</Box>
                    <Box style={styles.calo}>{Math.floor(exercise.calo/1000)}kcal</Box>
                  </Grid>
                  <Grid style={styles.time}>
                    <Box>{Math.floor(exercise.duration/60)} min</Box>
                  </Grid>
                </Box>
              </Grid>
            )
          })}
        </Grid>
      </Grid>
    </Box>
    </>
  )
}

const styles = {
  boxContainer: {
    backgroundColor: colors.dark500,
    padding: 25,
  },
  tableContainer: {
    height: 300,
    overflowY: 'scroll',
  },
  tableDetailItem: {
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 15,
    marginRight: 25,
    paddingBottom: 10,
    borderBottom: `1px solid ${colors.gray400}`,
    display: 'flex' as any,
    alignItems: 'center',
    flexDirection: 'row',
  },
  tableItem: {
  },
  headerLeft: {
    color: colors.light,
    fontSize: 15,
    marginLeft: 15,
  },
  headerRight: {
    color: colors.light,
    marginLeft: 30,
    fontSize: 22,
  },
  containerHeader: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: 10,
  },
  typeExercise: {
    color: colors.light,
    fontSize: 15,
    marginBottom: 7,
    display: 'list-item' as any,
    marginLeft: 10,
  },
  calo:{
    color: colors.primary300,
  },
  mainInfo: {
    flex: 1,
  },
  time: {
    color: colors.primary300,
    fontSize: 18,
  }, 
}