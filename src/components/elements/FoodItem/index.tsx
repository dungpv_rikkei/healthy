import React from 'react'
import { Box } from '@mui/material'
import { IFoodAttributes, getFoodTypeLabel } from '~/models/food'
import { colors } from '~/theme/variables/platform'

interface Props {
  food: IFoodAttributes
  style?: React.CSSProperties
}

export default function FoodItem({ food, style = {} }: Props) {

  const createAt = new Date(new Date(food.created_at))
  let hour = String(createAt.getHours()).padStart(2, '0')
  let minute = String(createAt.getMinutes()).padStart(2, '0')

  return (
    <Box style={{ ...styles.foodContainer, ...style }}>
      <img style={styles.image} src={food.image} />
      <span style={styles.label}>{`${hour}:${minute}`} {getFoodTypeLabel(food.type)}</span>
    </Box>
  )
}

const styles = {
  foodContainer: {
    position: 'relative' as any,
  },
  label: {
    position: 'absolute' as any,
    bottom: 5,
    left: 0,
    backgroundColor: colors.primary300,
    paddingLeft: 8,
    paddingTop: 8,
    paddingBottom: 8,
    paddingRight: 25,
    color: colors.light,
  },
  image: {
    maxWidth: '100%',
    height: 'auto',
  },
}