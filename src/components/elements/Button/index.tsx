import React from 'react'
import Button from '@mui/material/Button'
import { colors } from '~/theme/variables/platform'

interface Props {
  title: string
  onClick: () => void
}

const Index: React.FC<Props> = ({ title, onClick }) => {

  return (
    <Button style={styles.button} onClick={onClick}>{title}</Button>
  )
}

const styles = {
  button: {
    backgroundImage: `linear-gradient(to right, ${colors.primary300}, ${colors.primary400})`,
    color: colors.light,
    paddingLeft: 76,
    paddingRight: 76,
    paddingTop: 15,
    paddingBottom: 15,
    fontSize: 18,
  }
}

export default Index