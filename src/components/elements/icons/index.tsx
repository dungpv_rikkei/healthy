export function Logo({ width = 110, height = 40 }) {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={width} height={height} viewBox="151 405 669 253">
      <g data-name="グループ 134">
        <path d="M245.069 473.283h56.194v46.23h21.4v-114.35h-21.4v48.19H245.07v-48.19h-21.563v114.35h21.563v-46.23Z" fill="#ff963c" fillRule="evenodd" data-name="パス 82" />
        <path d="m416.36 508.405-12.742-12.579a18.516 18.516 0 0 1-4.432 3.265c-4.97 2.722-11.68 4.25-17.131 4.25a33.604 33.604 0 0 1-9.364-1.24c-8.501-2.466-13.665-8.4-14.65-15.913h62.892a84.887 84.887 0 0 0 .317-4.816c.945-25.925-11.26-40.686-31.165-44.19a56.703 56.703 0 0 0-9.827-.818 47.553 47.553 0 0 0-20.118 4.162c-14.26 6.611-22.682 20.564-22.682 37.984a47.547 47.547 0 0 0 4.617 21.287c4.933 9.931 13.68 16.966 25.562 20.003a56.86 56.86 0 0 0 14.091 1.674 56.178 56.178 0 0 0 8.725-.694c9.012-1.422 18.025-5.045 24.485-10.987a32.745 32.745 0 0 0 1.421-1.388Zm-14.54-38.716h-43.452a20.895 20.895 0 0 1 5.423-9.402c3.461-3.402 8.096-5.422 13.372-6.06a31.239 31.239 0 0 1 3.748-.22 28.95 28.95 0 0 1 9.119 1.346c6.513 2.166 10.867 6.944 11.79 14.336Z" fill="#ff963c" fillRule="evenodd" data-name="パス 83" /><path d="m501.827 507.588.98 11.762h18.786v-80.535H502.48l-.653 11.108a25.064 25.064 0 0 0-11.34-9.946 34.05 34.05 0 0 0-14.307-3.123 56.407 56.407 0 0 0-.391-.001c-18.899 0-34.464 9.53-39.702 27.82a51.94 51.94 0 0 0-1.89 14.328 53.295 53.295 0 0 0 2.709 17.47c5.774 16.694 20.26 25.33 38.391 25.33a57.32 57.32 0 0 0 .393-.001 32.47 32.47 0 0 0 4.677-.441c7.54-1.247 16.069-5.124 20.384-11.85a18.246 18.246 0 0 0 1.076-1.921Zm-13.836-50.952a24.785 24.785 0 0 0-10.015-1.975 22.749 22.749 0 0 0-21.752 13.5 27.242 27.242 0 0 0-2.097 10.84 28.27 28.27 0 0 0 1.44 9.155 22.717 22.717 0 0 0 22.41 15.348 25.39 25.39 0 0 0 8.658-1.436c19.357-6.979 19.809-37.36 1.356-45.432Z" fill="#ff963c" fillRule="evenodd" data-name="パス 84" />
        <path d="M538.281 405.163h19.766V519.35h-19.766V405.163" fill="#ff963c" fillRule="evenodd" data-name="長方形 45" />
        <path d="M701.518 477.04v42.31h19.93v-42.473c0-20.082-7.28-34.292-23.432-38.153a41.108 41.108 0 0 0-9.566-1.053c-9.138 0-17.95 2.771-25.133 12.057a38.627 38.627 0 0 0-.024.032V405h-19.93v114.35h19.93v-41.166a23.962 23.962 0 0 1 4.234-13.888 18.91 18.91 0 0 1 15.695-8.166 19.156 19.156 0 0 1 10.364 2.682c4.306 2.723 7.174 7.605 7.802 15.034a37.93 37.93 0 0 1 .13 3.194Z" fill="#ff963c" fillRule="evenodd" data-name="パス 85" />
        <path d="m632.15 516.899-5.554-16.989a24.532 24.532 0 0 1-5.468 1.949c-1.148.26-2.296.427-3.397.482a15.62 15.62 0 0 1-.773.02 11.417 11.417 0 0 1-5.396-1.22c-3.141-1.669-5.059-5.057-5.059-10.216V456.13h22.38v-17.152h-22.216v-19.64h-19.93v19.64h-15.029v17.152h15.029v34.795a40.52 40.52 0 0 0 1.543 11.588c3.674 12.312 13.664 18.33 27.371 17.817 5.316-.144 9.62-.92 14.47-2.66a60.372 60.372 0 0 0 2.03-.771Z" fill="#ff963c" fillRule="evenodd" data-name="パス 86" />
        <path d="m798.206 436.853-22.576 52.77-22.479-52.77h-21.78l33.346 78.278-46.845 109.498-81.994-72.424-484.878.043v20.033l477.353.009 96.72 85.71L820 436.853h-21.794Z" fill="#ff963c" fillRule="evenodd" data-name="パス 87" />
      </g>
    </svg>
  )
}

export function Hamburger() {
  return (
    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="33px" height="33px" viewBox="0 0 33 33" enableBackground="new 0 0 33 33" >
      <image id="image0" width="33" height="33" x="0" y="0"
        href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAhBAMAAAClyt9cAAAABGdBTUEAALGPC/xhBQAAACBjSFJN
      AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAG1BMVEUAAAD/lzz/lTr+ljv8
      mDv/lT3/kzz/ljz////7WJnyAAAAB3RSTlMAlcrkSmWAakhfaQAAAAFiS0dECIbelXoAAAAHdElN
      RQfmAxsWFBDnw+CCAAAAJ0lEQVQoz2NgoCNgVC9HAkVAESZzZJFirCIsYWlIIHXUPXRzD1kAAB9r
      SKDcOE7yAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDIyLTAzLTI3VDE5OjIwOjE2KzAzOjAwV2aJfAAA
      ACV0RVh0ZGF0ZTptb2RpZnkAMjAyMi0wMy0yN1QxOToyMDoxNiswMzowMCY7McAAAAAASUVORK5C
      YII=" />
    </svg>
  )
}

export function IconEdit() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="27" height="26.001" viewBox="3 3 27 26.001">
      <g data-name="グループ 139">
        <path d="m29.55 10.665-.993-.99a1.529 1.529 0 0 0-.784-.422 1.541 1.541 0 0 0-1.393.421l-.928.929 3.17 3.168.927-.927c.575-.576.6-1.494.075-2.099a1.564 1.564 0 0 0-.075-.08Z" fill="#ff963c" fillRule="evenodd" data-name="パス 43" /><path d="M16.76 19.287v3.17h3.17l7.827-7.828-3.17-3.17-7.827 7.828Z" fill="#ff963c" fillRule="evenodd" data-name="パス 44" /><path d="M9.347 9.934H18.3v1.492H9.347V9.934" fill="#ff963c" fillRule="evenodd" data-name="長方形 12" /><path d="M9.347 15.398H18.3v1.492H9.347v-1.492" fill="#ff963c" fillRule="evenodd" data-name="長方形 13" /><path d="M9.366 20.863h5.47v1.492h-5.47v-1.492" fill="#ff963c" fillRule="evenodd" data-name="長方形 14" /><path d="M22.177 27.013H5.367a.414.414 0 0 1-.409-.414V5.403a.414.414 0 0 1 .409-.416h16.81a.413.413 0 0 1 .409.416v5.987l1.958-1.987v-4A2.385 2.385 0 0 0 22.177 3H5.367C4.059 3.001 3 4.076 3 5.403v21.196c.001 1.326 1.06 2.4 2.367 2.401h16.81a5.903 5.903 0 0 0 1.053-.078c.779-.145 1.313-.53 1.314-1.474v-7.5l-1.958 1.988v4.663a.413.413 0 0 1-.409.414Z" fill="#ff963c" fillRule="evenodd" data-name="パス 45" />
      </g>
    </svg>
  )
}

export function IconChallenge() {
  return (
    <img src="/images/icon_challenge.png" />
  )
}

export function IconInfo() {
  return (
    <img src="/images/icon_info.png" />
  )
}

export function HexagonIcon({width = 116, height = 135, style}) {
  return (
    <svg style={style} xmlns="http://www.w3.org/2000/svg" width={width} height={height} viewBox="10 1 116 134"><path d="M100.5 0 134 58l-33.5 58h-67L0 58 33.5 0h67Z" fillRule="evenodd" fill="url(&quot;#a&quot;)" transform="rotate(-90 72.5 62.5)" data-name="パス 48" />
      <defs><linearGradient x1=".267" y1="0" x2=".715" y2="1" id="a">
        <stop stopColor="#ffcc21" offset="0" />
        <stop stopColor="#ff963c" offset="1" />
      </linearGradient></defs>
    </svg>
  )
}

export function  Spoon() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="53" height="45" viewBox="2 6 53 45"><g data-name="グループ 12"><g data-name="グループ 11"><path d="m14.981 50.185 8.702-8.703-3.957-3.957-8.702 8.704a2.766 2.766 0 0 0-.77 1.461 2.8 2.8 0 0 0 .775 2.489 2.794 2.794 0 0 0 1.401.76 2.787 2.787 0 0 0 2.42-.632 2.823 2.823 0 0 0 .131-.122Z" fill="#fff" fillRule="evenodd" data-name="パス 8"/><path d="m54.28 13.373-7.849 7.876-1.311-1.315 7.59-8.14-.736-.738-8.167 7.56-1.313-1.316 7.623-8.105-.793-.797-8.141 7.585-1.313-1.318 7.849-7.877L46.935 6a426.697 426.697 0 0 0-.146.115c-1.055.83-7.547 5.948-10.893 8.7a4.904 4.904 0 0 0-1.648 3.74v.096a5.931 5.931 0 0 0 .034.64c.239 2.166.5 3.046-.31 3.925a2.884 2.884 0 0 1-.077.08l-7.656 7.68a139389.571 139389.571 0 0 0-2.594-2.604C14.609 19.302 5.57 10.233 4.562 9.235a1.758 1.758 0 0 0-.457-.33 1.463 1.463 0 0 0-1.671.233 1.546 1.546 0 0 0-.04.039 1.33 1.33 0 0 0-.335.675c-.713 3.3 5.093 14.399 12.553 23.823a824.51 824.51 0 0 1 1.217-1.225c1.145-1.15 2.01-2.014 2.01-2.014l19.683 19.75a2.817 2.817 0 0 0 .68.506c1.13.598 2.473.27 3.245-.506h.001a2.775 2.775 0 0 0 .768-1.47 2.79 2.79 0 0 0-.768-2.47 8463.87 8463.87 0 0 1-1.664-1.671l-5.01-5.03a491436.156 491436.156 0 0 1-4.598-4.618l7.648-7.675a2.883 2.883 0 0 1 .126-.119c.857-.762 1.71-.517 3.748-.273a42.945 42.945 0 0 0 .118.014c.283.034.57.047.859.04a4.851 4.851 0 0 0 3.692-1.66c2.706-3.38 7.66-9.882 8.508-10.997a388.569 388.569 0 0 0 .125-.164l-.72-.72Z" fill="#fff" fillRule="evenodd" data-name="パス 9"/></g></g></svg>
  )
}

export function Snack() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="34" height="40" viewBox="11 8 34 40"><g data-name="グループ 23"><path d="m12.7 18 5.207 30H38.2l5.1-30h-30.6ZM40.904 8H15.149A4.101 4.101 0 0 1 11 12.046v2.621h34v-2.62a4.056 4.056 0 0 1-4.096-3.993 4.23 4.23 0 0 1 0-.054Zm-1.618 13.326L38.152 28H17.954l-1.134-6.674h22.466ZM20.787 44.667 19.653 38h16.8l-1.133 6.667H20.787Z" fill="#fff" fillRule="evenodd" data-name="coffee-15"/></g></svg>
  )
}

export function IconArrowTop({ style, onClick }) {
  return (
    <svg onClick={onClick} style={style} xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="1136 736 48 48">
      <g data-name="component_scroll">
        <g data-name="パス 92">
          <path d="M1160 736c13.255 0 24 10.745 24 24s-10.745 24-24 24-24-10.745-24-24 10.745-24 24-24Z" fill="rgba(255,255,255,0 )" fillRule="evenodd" />
          <path d="M1160 736.5c12.979 0 23.5 10.521 23.5 23.5s-10.521 23.5-23.5 23.5-23.5-10.521-23.5-23.5 10.521-23.5 23.5-23.5Z" strokeLinejoin="round" strokeLinecap="round" stroke="#777" fill="transparent" strokeWidth=".97917" />
        </g><path d="m1166.585 764.042-6.585-6.385-6.585 6.385-.876-.85 7.461-7.234 7.461 7.234-.876.85Z" fill="#777" fillRule="evenodd" data-name="パス 62" />
      </g>
    </svg>
  )
}