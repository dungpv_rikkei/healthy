import { Box } from '@mui/material'
import React from 'react'
import { TYPE_FOOD } from '~/models/food'
import { HexagonIcon, Spoon, Snack } from '../icons'
import { colors } from "~/theme/variables/platform"

interface Props {
  typeFood: TYPE_FOOD,
  title: string,
  customStyle?: React.CSSProperties
  onClick?: () => void
}

const styles = {
  boxContainer: {
    position: 'relative' as any,
    display: 'inline-block',
  },
  iconContainer: {
    position: 'absolute' as any,
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)',
    textAlign: 'center',
  },
  title: {
    color: colors.light,
    position: 'relative' as any,
    top: 5,
  },
  hexagonIcon: {
    maxWidth: 164,
    minWidth: 115,
  },
}

export default function MenuFoodItem({ typeFood, title, customStyle = {}, onClick }: Props) {

  return (
    <>
      <Box style={customStyle} onClick={onClick}>
        <Box style={styles.boxContainer}>
          <HexagonIcon style={styles.hexagonIcon}/>
          <Box style={styles.iconContainer as any}>
            {typeFood === TYPE_FOOD.SNACK ? (
              <Snack />
            ) : (
              <Spoon />
            )}
            <Box style={styles.title}>{title}</Box>
          </Box>
        </Box>
      </Box>
    </>
  )
}

