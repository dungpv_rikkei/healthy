import { Box } from '@mui/material'
import React from 'react'
import { colors } from '~/theme/variables/platform'

interface Props {
  category: ICategoryItem
}

export interface ICategoryItem {
  title?: string
  subTitle?: string
  image?: string,
  onClick?: () => void
}

const Index: React.FC<Props> = ({ category }) => {
  const { title, subTitle, image, onClick } = category
  return (
    <Box style={{paddingLeft: 15}}>
      <Box onClick={onClick} style={styles.categoryContainer}>
      <img style={styles.image} src={image} />
        <Box style={styles.contentCategory}>
          <Box style={styles.title}>{title}</Box>
          <Box style={styles.subTitle}>{subTitle}</Box>
        </Box>
      </Box>
    </Box>
  )
}

export default Index

const styles = {
  categoryContainer: {
    border: `15px solid ${colors.primary300}`,
    position: 'relative' as any,
    display: 'inline-block' as any,  
    backgroundColor: colors.dark600,
  },
  image: {
    maxWidth: '100%',
    height: 'auto',
  },
  contentCategory: {
    position: 'absolute' as any,
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)',
    width: 180,
  },
  title: {
    color: colors.primary300,
    fontSize: 23,
    textAlign: 'center' as any,
  },
  subTitle: {
    position: 'relative' as any,
    top: 15,
    fontSize: 14,
    backgroundColor: colors.primary400,
    paddingTop: 7,
    paddingBottom: 7,
    paddingLeft: 17,
    paddingRight: 17,
    textAlign: 'center' as any,
    color: colors.light,
  },
}