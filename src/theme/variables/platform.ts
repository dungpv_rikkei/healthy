
export const colors = {
  light: '#ffffff',
  primary300: '#ffcc21',
  primary400: '#ff963c',
  primary500: '#ea6c00',
  secondary300: '#8fe9d0',
  dark600: '#2e2e2e',
  dark500: '#414141',
  gray400: '#777777',
  
}

export const fontSize = {
  xLargeFontSize: 20,
  largeFontSize: 18,
  sLargeFontSize: 17,
  fontSize: 16,
  smallFontSize: 12,
  xSmallFontSize: 10,
  xxSmallFontSize: 8,
  placeholderFontSize: 14,
}
