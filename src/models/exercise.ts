
export interface IExerciseAttributes {
  calo: number
  type: number
  duration: number
}

export enum TYPE_OF_EXERCISE {
  EASY = 1,
  HEAVY = 2
}

export function getExerciseTypeLabel(exerciseType: TYPE_OF_EXERCISE) {

  if (exerciseType === TYPE_OF_EXERCISE.EASY) {
    return '家事全般（立位・軽い）'
  }
  return ''
}