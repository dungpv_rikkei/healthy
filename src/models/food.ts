export enum TYPE_FOOD {
  MORNING = 1,
  LUNCH = 2,
  DINNER = 3,
  SNACK = 4,
}

export interface  IFoodAttributes {
  type: TYPE_FOOD,
  image: string,
  created_at: string | Date
}

export function getFoodTypeLabel(foodType: TYPE_FOOD) {
  switch (foodType) {
    case TYPE_FOOD.MORNING:
      return 'Morning'
    case TYPE_FOOD.LUNCH:
      return 'Lunch'
    case TYPE_FOOD.DINNER:
      return 'Dinner'    
    case TYPE_FOOD.SNACK:
        return 'Snack'
  }
}