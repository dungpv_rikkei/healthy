export interface IBodyRecordAttributes {
  time?: string | Date
  calo?: number
  spo2?: number
}

export const DATA_TEMP_GRAPH = {
  labels: [
    '６月',
    '7月',
    '8月',
    '9月',
    '10月',
    '11月',
    '12月',
    '1月',
    '2月',
    '3月',
    '4月',
    '5月',
  ],
  datasets: [
    {
      fill: false,
      lineTension: 0.5,
      backgroundColor: '#ffcc21',
      borderColor: '#ffcc21',
      borderWidth: 2,
      data: [
        950,
        900,
        850,
        800,
        750,
        700,
        650,
        600,
        550,
        500,
        450,
        400
      ]
    },
    {
      fill: false,
      lineTension: 0.5,
      backgroundColor: '#8fe9d0',
      borderColor: '#8fe9d0',
      borderWidth: 2,
      data: [
        950,
        920,
        800,
        780,
        730,
        700,
        670,
        600,
        500,
        450,
        300,
        320
      ]
    }
  ]
}