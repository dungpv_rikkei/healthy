declare let window

export interface IUser {
  id: string
  name: string
  age?: number | null
  phone: string
}

const LOCAL_STORE_ME = 'me'

export function getMe() {
  const me = window?.localStorage.getItem(LOCAL_STORE_ME)
  return me ? JSON.parse(me) : null
}

export function storeMe(me: IUser) {
  window?.localStorage.setItem(LOCAL_STORE_ME, JSON.stringify(me))
}

export function clearMe() {
  window?.localStorage.removeItem(LOCAL_STORE_ME)
}