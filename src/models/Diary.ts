export interface IDiary {
  note: string
  created_at: string | Date
}