const path = require('path')
const withPlugins = require('next-compose-plugins')

module.exports = withPlugins(
  [],
  {
    eslint: {
      ignoreDuringBuilds: true,
    },
    typescript: {
      ignoreBuildErrors: true,
    },
    trailingSlash: true,
    serverRuntimeConfig: {

    },
    publicRuntimeConfig: {
      
    },
    webpack: (config, options) => {
      const { isServer } = options
      const assetPrefix = ''
      const inlineImageLimit = 8192
      config.resolve.fallback = {
        fs: false,
        tls: false,
        module: false,
        dgram: false,
        dns: false,
        http2: false,
        net: false,
        child_process: false,
        path: false,
        crypto: false,
      }
      // Images loader
      config.module.rules.push({
        test: /\.(jpe?g|png|svg|gif|ico|webp)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: inlineImageLimit,
              fallback: 'file-loader',
              publicPath: `${assetPrefix}/_next/static/images/`,
              outputPath: `${isServer ? '../' : ''}static/images/`,
              name: '[name]-[hash].[ext]',
            },
          },
        ],
      })

      config.module.rules.push({
        test: /\.(ttf|woff|woff2)$/,
        use: {
          loader: 'url-loader',
        },
      })

      config.plugins = config.plugins.filter((plugin) =>
        (plugin.constructor.name !== 'ForkTsCheckerWebpackPlugin')
      )
      config.watchOptions = {
        aggregateTimeout: 300,
        poll: 5000,
        ignored: [
          '**/.git',
          '**/.next',
          '**/node_modules',
          '**/public/static',
        ],
      }
      return config
    },
  }
)