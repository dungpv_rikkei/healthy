import type { AppProps } from 'next/app'
import Head from 'next/head'
import Layout from '~/components/layouts/main'
import { IconArrowTop } from '~/components/elements/icons'

function MyApp({ Component, pageProps }: AppProps) {

  const moveToTop = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' })
  }

  return (
    <Layout>
      <>
        <Head>
        </Head>
        <Component {...pageProps} />
        <IconArrowTop onClick={moveToTop} style={{position: 'fixed', cursor: 'pointer', top: '80%', right: 35}} />
      </>
    </Layout>
  )
}

export default MyApp
