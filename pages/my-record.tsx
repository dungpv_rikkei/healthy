import type { NextPage } from 'next'
import Head from 'next/head'
import MyRecordScreen from '~/components/screens/MyRecord'

const MyRecord: NextPage = () => {
  return (
    <div>
      <Head>
        <title>Healthy|MyRecord</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <MyRecordScreen />
    </div>
  )
}

export default MyRecord
