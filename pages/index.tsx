import type { NextPage } from 'next'
import Head from 'next/head'
import IndexScreen from '~/components/screens/Index'

const Home: NextPage = () => {
  return (
    <div>
      <Head>
        <title>Healthy</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <IndexScreen/>
    </div>
  )
}

export default Home
